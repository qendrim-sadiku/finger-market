#What is used

Includes:

* Gulp
* Boostrap
* Sass

## Getting Started

1. Install Node
2. Install gulp-sass
3. Install node-sass
4. Install Bower
```
npm install gulp-sass -g
npm install node-sass -g
npm install bower -g 
bower install
```

1. Clone this repo
1. Start your server

```
$ gulp serve
```
